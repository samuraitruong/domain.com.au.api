export interface Agent {
  agencyId?:           number;
  agentId?:            number;
  email?:              string;
  firstName?:          string;
  mobile?:             string;
  photo?:              string;
  lastName?:           string;
  phone?:              string;
  secondaryEmail?:     string;
  facebookUrl?:        string;
  twitterUrl?:         string;
  agentVideo?:         string;
  profileText?:        string;
  googlePlusUrl?:      string;
  personalWebsiteUrl?: string;
  linkedInUrl?:        string;
  mugShotURL?:         string;
  contactTypeCode?:    number;
  profileUrl?:         string;
}
