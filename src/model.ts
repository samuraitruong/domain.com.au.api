import { ListingType, PropertyTypes } from './enum';

export interface ListingSearchRequest {
  listingType?:   ListingType;
  propertyTypes?: PropertyTypes[];
  minBedrooms?:   number;
  minBathrooms?:  number;
  minCarspaces?:  number;
  locations?:     Location[];
}

export interface Location {
  state?:                     string;
  region?:                    string;
  area?:                      string;
  suburb?:                    string;
  postCode?:                  string;
  includeSurroundingSuburbs?: boolean;
}

export interface PageRequest {
  pageNumber?: number;
  pageSize?:number    
}
export interface ListingLocationRequest extends PageRequest {
  terms:string
}
export interface AgencySearchRequest extends PageRequest {
  q:string
}

export interface AgentSearchRequest extends PageRequest {
  query:string
}

